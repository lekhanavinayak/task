package com.employeeMan.EmployeeMan;

import com.employee.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;
@NoRepositoryBean
@Repository
public interface EmployeeRepo extends CrudRepository<Employee,Integer> {
}
