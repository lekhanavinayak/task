package com.employeeMan.EmployeeMan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication

public class EmployeeManApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeManApplication.class, args);
	}

}
