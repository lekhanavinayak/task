package com.service;

import com.employeeMan.EmployeeMan.EmployeeRepo;
import com.employee.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {


    @Autowired
    EmployeeRepo employeeRepo;


    public void addUser(Employee employee) {
        employeeRepo.save(employee);
    }
}
