package com.emp.emp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmpService {
    @Autowired
    EmployeeRepo employeeRepo;


    public void addUser(Employee employee) {
        employeeRepo.save(employee);
    }
}
