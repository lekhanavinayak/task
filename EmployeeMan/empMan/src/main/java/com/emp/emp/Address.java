package com.emp.emp;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;
@Entity
public class Address {
    @NotBlank(message = "address is mandatory")
    private String address;
    private String city;
    @NotBlank(message = "country name is mandatory")
    private String country;

    private Employee id;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Employee getId() {
        return id;
    }

    public void setId(Employee id) {
        this.id = id;
    }

}
