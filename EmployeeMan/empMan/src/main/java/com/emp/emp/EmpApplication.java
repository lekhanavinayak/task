package com.emp.emp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com/address/springallrelation/onetomany","com/controller/springallrelation/onetomany","com/employee/springallrelation/onetomany","com/Repo/springallrelation/onetomany","com/service/springallrelation/onetomany","com/employeeMan/EmployeeMan/springallrelation/onetomany"})
public class EmpApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmpApplication.class, args);
	}

}
